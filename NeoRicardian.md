# Smart Contract Money

<p align="center">
  <img src="./graphics/NeoRicardian.jpg">
</p>

## We need to begin thinking of money under different terms. "Smart" Money equals smart contracts.  Instead, let's begin to think of money in terms of the supply chain in which it works. We're moving away from vertical integrations that corporatism gave us, and toward a system of horizontal cooperation that crypto currencies provide. We need to begin thinking like Vitalik Buterin who says "Money is something a community determines it should use." Chainlink, Ethererum an other cryptocurrencies represent a community of interests who overlap.  We're at the beginning of money which has integrity built-in. In Chainlink's case, they insure suppliers of [Oracle information](https://chain.link/features/) maintain integrity by building-in insurance. Providers of information, such as health care workers, insure the information they provide by using the Chainlink protocol. Therefore, Chainlink can ensure an endpoint such as an ["attestation" terminal](https://gitlab.com/Crypteriat/jitterbug) sending information it certifies. Just like companies require bonded individuals working in their facilities, they will require bonded information.
