# Introduction

This project was originally envisioned to support a World Health Organization tokenized crypto ecosystem to prevent unreliable or incomplete pandemic or outbreak health information in the future.
In a way, we were focused on creating a "Hong Kong proof" voting and accountability system. We envisioned a user experience whereby individuals could load an "ephemeral app" onto their
mobile device, use it to physically vote, or check-in via a decentralized terminal network. Then they could simply delete the app from their phone so that individuals might be able to physically
and virtually organize with low risk to detection by authorities. The core concept allowed for a network physical or virtual voting capability. Use of a terminal, for example, might allow a certifier to 
"virtually" setup a network to accept government identification or some other means (e.g. social security #, shared or ZKP style-secret). We now think it is time to generalize our previous architecture
and leverage the analyses to create an up to date technological current membership and voting systems. 

We propose to create a general architecture that might be leveraged by governments, unions, or other groups which need to establish a virtual organization. Thus, we have three steps,
initially in the process:
1. Accept and normalize election data from public and private information feeds. Public open sourced data is critical the confidence for members of any organization, inlcuding nation states. 
2. Create a system for revisioning of the data.
3. Enable analyses and visualization of the data sets via the ephemeral app. In other words, we intend to add web based tools that allow all members to see the data. Why should third parties be
intermediaries for an organizations membership. Integrity and transparency should be built into the system.

Our initial goal is to provide a normalized structure for our public election information. We are currently working on the initial step only. We will follow through with the ephemeral app work
after we can obtain normalized data sets for the 2020 U.S. national election. We see this as as the first national election where the fight is going to be over big data, and we need to provide the
tools form the bleeding edge of the crypto community to decision makers in our electoral system.

## Initial Data Organization

Fundamentally, membership in any organization by an individuals qualification to join. In addition, organizations typically need to keep some attributes for the individual members as well as how they
may have conducted the membership and voting transactions. We propose the following for structuring the data:

![Presumed Data Organization](./graphics/voting_types.svg) 

![Independent of Certification](./graphics/virtual_voting.svg) 

## Merkelized Revisions

![Revisions Integrity Guarantee](./graphics/virtual_voting.svg) 

## Data Feed Organization

![Data Feed](./graphics/data_feed.svg) 

Oracles represent sources of information to blockchains. In this case, our oracles represent human beings who must be able to enter information regularly into their health care systems.
Unfortunately, the current pandemic's information was widely corrupted and suppressed at the early stages when it was simmply an epidemic.
Humans must be able to vote on the quality of the information leaving their insitutions.  We believe this problem can be solved with one of the core blockchain applications:
verifiable honest voting.  It is now possible to establish truly anonymous voting systems that with corresponding GPS-verified polling stations can group individuals into
organizations. Therefore, through proper gamified financial and social incentives, an information chain may be established that ensures integrity. This chain's information is
incredibly valuable -- not only to the public. Investors, other health organizations, prediction markets, and others will be willing to pay for subscriptions. 

We have an opportunity to insure information integrity through tokenization. By creating a financial incentive for medical personnel to submit and vote on the information coming out of
healthcare facilities. Therefore, the canaries in the coal mine will always be heard via big data analytics. In addition, it is 
possible to solve this problem with the creation of a new smart contract on existing blockchains or via usage of existing protocols such as Chainlink.
We can only attempt to solve the problem at hand and are asking other interested parties for their thoughts on the collaboration. Our Readme & Whitepaper represent work in progress
and are subject to rapid change.  Integrity of oracles is one of the core problems facing the crypto ecosystem today. Fortunately,
a number of solutions have emerged in the past several years. Crypteriat intends to leverage existing technologies as much as possible in our solutions map.



## Creating a new Financial Instrument & Information Source

We propose that this information is not completely free as public good. Hedge funds and other finanical institutions are natural consumers and can afford to pay. On the other hand, government orgainzations,
and NGOs cannot necessarily afford to pay. Thus, at least two tiers of information are required. A free tier can be provided directly to governments and NGOs digested big data analytics. Raw data can be
provided to others in exchange for a fee which will in turn pay back workers. These fees will be insured to be sufficient to information providers. We expect initial funding for the project to be provided
by governments and traditional corporations. This project should put blockchain on the government map as well as serving the public's interest. Consequently, this project demonstrates a way for governments
and citizens alike to benefit from blockchain technology.


## Initial Solution: APP + Physical Polling Infrastructure

We have already prototyped a Raspberry PI with NFC technology and a corresponding mobile phone app using React-Native for Android and iOS. This prototype could easily be adapted to other government
services such as water, sewer and power monitoring as well as extending into internal financial audits compliance. It is not just for government. We will naturally want to extend it into employees of
ordinary corporations. This may be scary to some, but we find it frighteningly right for our current times. We need to start seeing the world's problems as primarily technological instead of political.
The lack of information coming from a place like China should be viewed as a FinTech problem ideally not as a United Nation's one.
Hence, with about 2 more months of development, we think the prototype can be ready for alpha and beta trials.

![Proposed Solution](./graphics/oracle_filter.svg) 

## Mitigating against Malicious Opponents

Bad actors, including governments may attempt to thwart the system by influencing health care workers ability to either enter or verify information. We propose to leverage an economic model known as
[Quadratic Dispersment](https://vitalik.ca/general/2019/12/07/quadratic.html). We expect future [Neo-Ricardian](./NeoRicardian.md) crypto currencies to do the same.  By leveraging 
this approach, we can create a sustainable ecosystem based on the information and services it initially enabled. Therefore, there is no economic incentive to attack the project. Malicious actors who wish to manipulate financial markets and hackers will
will most certainly to attack or gain financially. However, there will be a greater financial investment in protecting the network. As such, we expect the gain in token sales will ultimately
ensure the security of the network.


## FAQ

* Is the World Health Organization involved with creating this token?

    Not currently. We are seeking to align with any interested party who would like to contribute funds or knowledge. Currently, we are focused on recruiting quality software engineers.

* How will this token work with and support the World Health Organization?

    We have not formalized a relationship. We plan to provide transparency to all NGOs, government organizations, as well individuals. We have concluded, given the failure of institutions in our current
    crisis, that a fundamentally non-institutional must be taken. The blockchain-cryptocurrency is uniquely advantaged to solve these sorts of problems.

* How can we be sure governments or other censorship-oriented actors do not intervene with the hardware or software 

    That question is fundamentally one that is impossible to answer. There will be a spectrum of responses. We could face restrictions like the Tor protocol does, or more optimistically, with the correct
    dopamine response games, we can gameify the reward with cryptocurrency. By developing the correct reward systems, we intend to apply what we've learned from social media and gambling forward. We 
    must leverage what we've learned and apply to more socially beneficial causes.


* How can we insure social pressure will not prevent care professionals from misreporting.   

    Again, it is impossible to answer. Ideally, people will use to associate themselves with credentialed Health Care Organizations. This may be one of the ways to approach identity and attestation on
    the blockchain. It could also be positioned as a new form of guild or union for members to join.

* How can we insure social pressure will not prevent care professionals from misreporting.   

* Is this a token offering of some sort?

    No. This project is using "commercial" off the shelf technology. That technology, of course, is not based on credentialed institutional offerings. This approach represents to current state
    of crypto projects.

* Is this a token offering of some sort?

    No. This project is using "commercial" off the shelf technology. That technology, of course, is not based on credentialed institutional offerings. This approach represents to current state
    of the art for crypto projects. We are leveraging existing blockchains and ecosystem components to provide a solution.

* What are the protocol and oracle risks for the project?

    All blockchain projects have a variety of inherent risks including oracle and protocol ones. Since this project leverages existing protocols and blockchains such as Chainlink, and Ethereum, with proper execution,
    we expect with proper implementation there to be no additional risks beyond what is already inherent with those technologies.

* How does Chainlink protect the integrity of information.

    [Smart Contracts: Neo-Ricardian Money](./NeoRicardian.md)

## Project Organization

[Terminal](https://gitlab.com/Crypteriat/jitterbug) ---------------------------------------------------------------------------------- [App](https://gitlab.com/Crypteriat/WebApps/mynapp)
<p align="center">
  <img width="460" height="300" src="./graphics/Organization.jpg">
</p>
[Blockchain API](https://gitlab.com/Crypteriat/Network_Bound_PoW/omoikane)


## Time is Critical

![We are Running out of Time ](./graphics/Outbreak.jpg) 


## Conclusion

We have an opportunity to create a truly scalable system worldwide for insuring the integrity of publiic data. Our
goal is not to capitalize on this opportunity. Instead, we wish to leverage all of the great infrastructure
already developed. Please consider donating. Our addresses are below.

### Donations:

If you donate via crypto currency, please email `jo` at `crypteriat.org` with your source wallet address so we can track your donation. We hope to provide you with some additional benefits as this project matures.

> <img width="300" height="300" alt="BTC" src="./graphics/BTC.png">
>  BTC - 3QfVhBcyCsi7gBvv9zVxvh8HnGfo1dEfUE

> <img width="300" height="300" alt="ETH" src="./graphics/ETH.png">
>  ETH - 0x6520a28adcd29C8E52007957160E5d2aEbF32a12

> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WTVXDGXPUYMF6&source=url"><img width="150" src="./docs/PayPal.png" alt="PayPal" /></a>

> <img width="300" height="300" alt="PayPal" src="./docs/PayPalQR.png">
> [Donate via PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WTVXDGXPUYMF6&source=url)
